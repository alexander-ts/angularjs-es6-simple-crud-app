export function routing($stateProvider) {

    $stateProvider
        .state('app.feature-b', {
            url: '/',
            template: '<todo-component></todo-component>'
        });
}