export default class TodoComponent {

    constructor($http) {
        this.$http = $http;
        this.getAllUsers();
    }

    getUser(id) {
        let self = this;
        this.$http({
            method : "GET",
            url : "http://localhost:8080/users/" + id
        }).then(function mySuccess(response) {
            console.log(response);
            self.user = response.data;
        }, function myError(response) {
            console.log(response);
        });
    }

    getAllUsers() {
        let self = this;
        this.$http({
            method : "GET",
            url : "http://localhost:8080/users/"
        }).then(function mySuccess(response) {
            console.log(response);
            self.users = response.data;
        }, function myError(response) {
            console.log(response);
        });
    }

    deleteUser(id) {
        let self = this;
        this.$http({
            method : "DELETE",
            url : "http://localhost:8080/users/" + id
        }).then(function mySuccess(response) {
            self.getAllUsers();
        }, function myError(response) {
            self.getAllUsers();
        });
    }

    createUser(user) {
        let self = this;
        this.$http({
            method : "POST",
            url : "http://localhost:8080/users/",
            data : user
        }).then(function mySuccess(response) {
            self.getAllUsers();
            self.cleanUserFields(user)
        }, function myError(response) {
            self.getAllUsers();
            self.cleanUserFields(user)
        });
    }

    updateUser(user) {
        let self = this;
        this.$http({
            method : "PUT",
            url : "http://localhost:8080/users/",
            data : user
        }).then(function mySuccess(response) {
            self.getAllUsers();
            self.cleanUserFields(user)
        }, function myError(response) {
            self.getAllUsers();
            self.cleanUserFields(user)
        });
    }

    cleanUserFields(user) {
        user.id = undefined;
        user.fname = undefined;
        user.lname = undefined;
        user.email = undefined;
    }
}
